package DAO;

import BlogApp.UserPOJO;
import com.sun.tools.internal.xjc.reader.xmlschema.bindinfo.BIConversion;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDAO implements AutoCloseable {

    private final Connection conn;
    //ADD HERE CONNECTION WHEN MERGE FILES

    private UserPOJO userFromResultSet(ResultSet rs) throws SQLException{
    return new UserPOJO(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getDate(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9));
    }



    public List<UserPOJO> getAllUserInfo() throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("select * from user")) {
            try (ResultSet rs = stmt.executeQuery()) {
                List<UserPOJO> userInfoList = new ArrayList<>();
                while (rs.next()) {
                    userInfoList.add(userFromResultSet(rs));
                }
                return userInfoList;
            }
        }
    }

    public UserPOJO getUserById(Integer id) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement("SELECT * FROM user WHERE id = ?")) {
            stmt.setInt(1, id);
            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return userFromResultSet(rs);
                } else {
                    return null;
                }
            }
        }
    }

    public void saveUser (UserPOJO user) throws SQLException {
        if (user.getId() == null) {
            addUser(user);
        } else {
            updateUser(user);
        }
    }

    public void addUser(UserPOJO user){
        if (user.getId() == null) {
            addUser_generateId(user);
        } else {
            addUser_supplyId(user);
        }
    }


    public void updateUser(UserPOJO user) throws SQLException{
        try (PreparedStatement stmt = conn.prepareStatement("UPDATE user SET  fname = ?, lname = ?, dateOfBirth = ?, username = ?, password = ?, country = ?, description = ?, image = ? WHERE id = ?")) {
            stmt.setString(2, user.getFname());
            stmt.setString(3, user.getLname());
            stmt.setDate(4,(Date)user.getDateOfBirth());
            stmt.setString(5,user.getUsername());
            stmt.setString(6,user.getPassword());
            stmt.setString(7, user.getCountry());
            stmt.setString(8, user.getDescription());
            stmt.setString(9, user.getAvatar());
            stmt.executeUpdate();

        }
    }

    public void addUser_generateId(UserPOJO user){
        try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO user (fname, lname, dateOfBirth, username, password, country, description, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
            stmt.setString(2, user.getFname());
            stmt.setString(3, user.getLname());
            stmt.setDate(4,(Date)user.getDateOfBirth());
            stmt.setString(5,user.getUsername());
            stmt.setString(6,user.getPassword());
            stmt.setString(7, user.getCountry());
            stmt.setString(8, user.getDescription());
            stmt.setString(9, user.getAvatar());

            stmt.executeUpdate();
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                rs.next();
                user.setId(rs.getInt(1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

     public void addUser_supplyId(UserPOJO user){

         if (user.getId() == null) {
             throw new IllegalArgumentException("article's id cannot be null.");
         }

         try (PreparedStatement stmt = conn.prepareStatement("INSERT INTO user (id, fname, lname, dateOfBirth, username, password, country, description, image) VALUES (?, ?, ?, ?, ?, ?, ?, ?)")) {
             stmt.setInt(1, user.getId());
             stmt.setString(2, user.getFname());
             stmt.setString(3, user.getLname());
             stmt.setDate(4,(Date)user.getDateOfBirth());
             stmt.setString(5,user.getUsername());
             stmt.setString(6,user.getPassword());
             stmt.setString(7, user.getCountry());
             stmt.setString(8, user.getDescription());
             stmt.setString(9, user.getAvatar());
             stmt.executeUpdate();
         } catch (SQLException e) {
             e.printStackTrace();
         }
     }

    public void deleteUser(int id) throws SQLException {
        try (PreparedStatement stmt = conn.prepareStatement("DELETE FROM user WHERE id = ?")) {
            stmt.setInt(1, id);
            stmt.executeUpdate();
        }
    }

    @Override
    public void close() throws Exception {

    }
}
